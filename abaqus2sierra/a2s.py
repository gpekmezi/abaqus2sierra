#!/usr/bin/python3

import sys, os, getopt
from abaqusParser import *
from exodusWriter import *

def main(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print('a2s.py -i <inputfile> -o <outputfile>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print('a2s.py -i <inputfile> -o <outputfile>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
        if os.path.isfile(arg):
          raise NameError('Output file exists. Remove and try again.')
        outputfile = arg
   print('Input file is "', inputfile)
   print('Output file is "', outputfile)
   
   fem = parse_model(inputfile)
   write_exodus(fem, outputfile)

if __name__ == "__main__":
   main(sys.argv[1:])