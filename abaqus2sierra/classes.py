import numpy as np

class Mesh:
  def __init__(self):
    self.nodes = {}
    self.node_sets = {}
    self.elements = {}
    # self.element_types = []
    self.element_sets = {}
    self.surface_sets = []
  class nodes:
    def __init__(self, coords):
      self.coords = np.array(tuple(coords),dtype=[('x', 'f8'),('y', 'f8'),('z', 'f8')])
  # class node_sets:
  #   def __init__(self, setname, setnodes):
  #     self.name = setname
  #     self.nodes = setnodes
  class elements:
    def __init__(self, eltype, vertices):
      self.type = eltype
      self.nodes = vertices
  # class element_sets:
  #   def __init__(self, setname, setelements):
  #     self.name = setname
  #     self.elements = setelements
  class surface_sets:
    def __init__(self, setname, surfelems, settype):
      self.name = setname
      self.elements = surfelems
      self.type = settype

class Model:
  def __init__(self, mpath, mname, mmesh):
    self.path = mpath
    self.name = mname
    self.mesh = mmesh
    self.materials = {}
    self.sections = {}
    self.boundary_conditions = []
    self.steps = []

class SolidSection:
  def __init__(self, etyp, matname):
    self.section_type = "Solid"
    self.eltype = etyp
    self.material_name = matname

class ShellSection:
  def __init__(self, etyp, matname):
    self.section_type = "Shell"
    self.eltype = etyp
    self.material_name = matname

class BeamSection:
  def __init__(self, etyp, matname, btyp):
    self.section_type = "Beam"
    self.eltype = etyp
    self.beam_type = btyp
    self.material_name = matname

class Material:
  def __init__(self, matname):
    self.name = matname

class Elastic_Isotropic:
  def __init__(self, elparams):
    self.kind = "ISOTROPIC"
    self.params = np.array(tuple(elparams),dtype=[('E', 'f8'),('nu', 'f8')])

class Elastic_Eng_Constants:
  def __init__(self, elparams):
    self.kind = "ENGINEERING CONSTANTS"
    self.params = np.array(tuple(elparams),
                            dtype=[('E1', 'f8'),('E2', 'f8'),('E3', 'f8'),
                                  ('nu12', 'f8'),('nu13', 'f8'),('nu23', 'f8'),
                                  ('G12', 'f8'),('G13', 'f8'),('G23', 'f8')])

class Elastic_Lamina:
  def __init__(self, elparams):
    self.kind = "LAMINA"
    self.params = np.array(tuple(elparams),
                            dtype=[('E1', 'f8'),('E2', 'f8'),('nu12', 'f8'),
                                  ('G12', 'f8'),('G13', 'f8'),('G23', 'f8')])

class Elastic_Anisotropic:
  def __init__(self, elparams):
    self.kind = "ANISOTROPIC"
    self.params = np.array(tuple(elparams),
                            dtype=[('C1111', 'f8'),('C1122', 'f8'),('C1133', 'f8'),
                                  ('C1144', 'f8'),('C1155', 'f8'),('C1166', 'f8'),
                                  ('C2222', 'f8'),('C2233', 'f8'),('C2244', 'f8'),
                                  ('C2255', 'f8'),('C2266', 'f8'),
                                  ('C3333', 'f8'),('C3344', 'f8'),('C3355', 'f8'),
                                  ('C3366', 'f8'),
                                  ('C4444', 'f8'),('C4455', 'f8'),('C4466', 'f8'),
                                  ('C5555', 'f8'),('C5566', 'f8'),
                                  ('C6666', 'f8')])

class Plastic_Isotropic:
  def __init__(self, elparams):
    self.kind = "ISOTROPIC"
    elparams = list(zip(*[iter(elparams)]*2))
    self.params = np.asarray(elparams,dtype=[('stress', 'f8'),('strain', 'f8')])

class Plastic_JohnsonCook:
  def __init__(self, elparams):
    self.kind = "JOHNSON COOK"
    self.params = np.array(tuple(elparams),
                            dtype=[('A', 'f8'),('B', 'f8'),('n', 'f8'),
                                  ('m', 'f8'),('thetam', 'f8'),('thetat', 'f8')])

class Boundary_Condition:
  def __init__(self, bckind, bcdata, bcoptions):
    self.kind = bckind
    self.data = bcdata
    self.options = bcoptions

class MPC:                     # Multi-Point Constraint
  def __init__(self, mpctype, mpcnodes):
    self.type = mpctype
    self.nodes = mpcnodes

class Coupling:                # Surface-based coupling links
  def __init__(self, cotype,coname,corefnode,cosurf,codofs):
    self.type = cotype      # Kinematic or Distributing
    self.constraint_name = coname
    self.ref_node = corefnode
    self.surface = cosurf
    self.dofs = codofs

class Keyword:
  def __init__(self, keyname, keyoptions):
    self.name = keyname
    self.options = keyoptions

class AbaqusReaderState:
  def __init__(self, stsection, stmaterial, stproperty, ststep, stdata):
    self.section = stsection
    self.material = stmaterial
    self.property = stproperty
    self.step = ststep
    self.data = stdata

class Step:
  def __init__(self, stepkind=None, stepbcs=[], stepout=[], opened=False):
    self.kind = stepkind  # STATIC, DYNAMIC implemented thus far
    self.boundary_conditions = stepbcs
    self.output_requests = stepout
    self.open = opened

class OutputRequest:
  def __init__(self, outkind, outdata, outopts, outtarget):
    self.kind = outkind # NODE, EL, SECTION, ...
    self.data = outdata
    self.options = outopts
    target = outtarget # PRINT, FILE
