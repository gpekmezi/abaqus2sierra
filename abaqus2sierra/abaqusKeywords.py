abaqus_keywords = []
mesh_keywords = []
unsupported_keywords = ["HEADING","PREPRINT","ORIENTATION","RESTART"]

ELEMENT_SECTIONS = ["SOLID SECTION","SHELL SECTION","BEAM SECTION"]
abaqus_keywords.extend(ELEMENT_SECTIONS)
# GENERAL_ELEMENT_SECTIONS not currently supported
GENERAL_ELEMENT_SECTIONS = ["SHELL GENERAL SECTION","BEAM GENERAL SECTION"]
abaqus_keywords.extend(GENERAL_ELEMENT_SECTIONS)

abaqus_keywords.append("MATERIAL")
abaqus_keywords.append("ELASTIC")
abaqus_keywords.append("PLASTIC")
abaqus_keywords.append("DENSITY")
# TRANSVERSE SHEAR STIFFNESS currently ignored
abaqus_keywords.append("TRANSVERSE SHEAR STIFFNESS")

abaqus_keywords.append("STEP")
abaqus_keywords.append("STATIC")
abaqus_keywords.append("DYNAMIC")
abaqus_keywords.append("END STEP")

abaqus_keywords.append("COUPLING")
abaqus_keywords.append("KINEMATIC")
abaqus_keywords.append("DISTRIBUTING")

abaqus_keywords.append("MPC")

BOUNDARY_CONDITIONS = ["BOUNDARY","CLOAD","DLOAD","DSLOAD"]
abaqus_keywords.extend(BOUNDARY_CONDITIONS)

OUTPUT_REQUESTS = ["SECTION PRINT","SECTION FILE","MONITOR","EL PRINT","EL FILE","ELEMENT OUTPUT",
                        "NODE PRINT","NODE FILE","OUTPUT", "NODE OUTPUT"]
abaqus_keywords.extend(OUTPUT_REQUESTS)

mesh_keywords.append("NODE")
mesh_keywords.append("ELEMENT")
MESH_SETS = ["NSET","ELSET"]
mesh_keywords.extend(MESH_SETS)
mesh_keywords.append("SURFACE")
MESH_GEN = ["NGEN","ELGEN","NFILL"]
mesh_keywords.extend(MESH_GEN)
