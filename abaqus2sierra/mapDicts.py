# Currently supported element types are listed below
# Those element types commented out may be implemented later
nodes_in_element = {  
                    'B31'    : 2 ,        #(EX)2-node Linear Beam
                    'B31H'   : 2 ,        #(EX)2-node Linear Beam (Hybrid)
                    'B33'    : 2 ,        #(EX)2-node Cubic Beam
                    'B33H'   : 2 ,        #(EX)2-node Cubic Beam (Hybrid)
                    'C3D4'   : 4 ,        #(EX)1st Order Tet
                    'C3D4H'  : 4 ,        #(EX)1st Order Tet (Hybrid)
                    'C3D6'   : 6 ,        #(EX)1st Order Wedge
                    'C3D8'   : 8 ,        #(EX)1st Order Hex
                    'C3D10'  : 10,        #(EX)2nd Order Tet
                    'C3D15'  : 15,        #(EX)2nd Order Wedge
                    'C3D20'  : 20,        #(EX)2nd Order Hex
                    'C3D20R' : 20,        #(EX)2nd Order Hex (Red. Int.)
                    # 'CPS4'     : 4 ,       #(EX)Membrane
                    'S3'     : 3 ,        #1st Order Tri shell
                    'S3R'    : 3 ,        #(EX)1st Order Tri shell (Red. Int.)
                    'S3RS'   : 3 ,        #1st Order Tri shell (Red. Int. & Hourglass)
                    'S3RS'   : 3 ,        #1st Order Tri shell (Red. Int. & Hourglass)
                    'S4'     : 4 ,        #(EX)1st Order Quad shell
                    'S4R'    : 4 ,        #(EX)1st Order Quad shell (Red. Int.)
                    'S4RS'   : 4 ,        #1st Order Quad shell (Red. Int. & Hourglass)
                    'S4RSW'  : 4 ,        #1st Order Quad shell (Red. Int., Hourglass, Warp.)
                    'S4R5'   : 4 ,        #(EX)1st Order Quad shell (Red. Int., 5 DOF)
                    'S8'     : 8 ,        #2nd Order Quad shell
                    'S8R'    : 8 ,        #(EX)2nd Order Quad shell (Red. Int.)
                    'S8RS'   : 8 ,        #2nd Order Quad shell (Red. Int. & Hourglass)
                    'S8R5'   : 8 ,        #(EX)2nd Order Quad shell (Red. Int., 5 DOF)
                    'STRI65' : 6 ,        #(EX)2nd Order Tri shell (Thin, 5 DOF)
                    # 'SPRING1': 1 ,        #1-node Spring 6 DOF
                    # 'SPRING2': 2 ,        #2-node Spring 6 DOF
                    # 'SPRINGA': 2 ,        #2-node Axial Spring
                    # 'T3D2'   : 2 ,        #2-node Truss/Rod
                    # 'T3D2H'  : 2 ,        #2-node Truss/Rod (Hybrid)
                    # 'RB3D2'  : 2 ,        #2-node Rigid Link
                    }

equiv_exo_element = {  
                    'B31'    : 'BEAM2'  ,
                    'B31H'   : 'BEAM2'  ,
                    'B33'    : 'BEAM2'  ,
                    'B33H'   : 'BEAM2'  ,
                    'C3D4'   : 'TET4'   ,
                    'C3D4H'  : 'TET4'   ,
                    'C3D6'   : 'WEDGE6' ,
                    'C3D8'   : 'HEX8'   ,
                    'C3D10'  : 'TET10'  ,
                    'C3D15'  : 'WEDGE15',
                    'C3D20'  : 'HEX20'  ,
                    'C3D20R' : 'HEX20'  ,
                    'S3'     : 'TRI3'   ,
                    'S3R'    : 'TRI3'   ,
                    'S3RS'   : 'TRI3'   ,
                    'S3RS'   : 'TRI3'   ,
                    'S4'     : 'QUAD4'  ,
                    'S4R'    : 'QUAD4'  ,
                    'S4RS'   : 'QUAD4'  ,
                    'S4RSW'  : 'QUAD4'  ,
                    'S4R5'   : 'QUAD4'  ,
                    'S8'     : 'QUAD8'  ,
                    'S8R'    : 'QUAD8'  ,
                    'S8RS'   : 'QUAD8'  ,
                    'S8R5'   : 'QUAD4'  ,
                    'STRI65' : 'TRI6'   ,
                    }

equiv_exo_conn = {  
                    'B31'    : list(range(2)) ,
                    'B31H'   : list(range(2)) ,
                    'B33'    : list(range(2)) ,
                    'B33H'   : list(range(2)) ,
                    'C3D4'   : list(range(4)) ,
                    'C3D4H'  : list(range(4)) ,
                    'C3D6'   : list(range(6)) ,
                    'C3D8'   : list(range(8)) ,
                    'C3D10'  : list(range(10)),
                    'C3D15'  : [*list(range(9)),*list(range(12,15)),*list(range(9,12))],
                    'C3D20'  : [*list(range(12)),*list(range(16,20)),*list(range(12,16))],
                    'C3D20R' : [*list(range(12)),*list(range(16,20)),*list(range(12,16))],
                    'S3'     : list(range(3)) ,
                    'S3R'    : list(range(3)) ,
                    'S3RS'   : list(range(3)) ,
                    'S3RS'   : list(range(3)) ,
                    'S4'     : list(range(4)) ,
                    'S4R'    : list(range(4)) ,
                    'S4RS'   : list(range(4)) ,
                    'S4RSW'  : list(range(4)) ,
                    'S4R5'   : list(range(4)) ,
                    'S8'     : list(range(8)) ,
                    'S8R'    : list(range(8)) ,
                    'S8RS'   : list(range(8)) ,
                    'S8R5'   : list(range(8)) ,
                    'STRI65' : list(range(6)) ,
                    }