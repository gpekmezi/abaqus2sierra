## The following command will load ACCESS and Python 3 on Onyx.
# module load cseinit cse/python3/latest SEACAS
## Check documentation for how to do this on your system, then set
## the $ACCESS environment variable as necessary

import sys, os
sys.path.insert(1, (os.environ.get('ACCESS')+'/lib') )
from exodus3 import *
from mapDicts import *

# from abaqusParser import *
# inputfile, outputfile = '../tests/inp_files/s4lamina.inp', '../tests/out_files/s4lamina.exo'
# fem = parse_model(inputfile)
# write_exodus(fem, outputfile)

def write_exodus(fem, outputfile):
  # Create EXODUS II file
  exo = exodus(outputfile,'w','numpy',fem.name,3,len(fem.mesh.nodes),
              len(fem.mesh.elements),len(fem.sections),
              len(fem.mesh.node_sets),len(fem.mesh.surface_sets))

  # Write nodal coordinates values and names to database
  exo.put_coords( [fem.mesh.nodes[node].coords['x'] for node in fem.mesh.nodes],
                  [fem.mesh.nodes[node].coords['y'] for node in fem.mesh.nodes],
                  [fem.mesh.nodes[node].coords['z'] for node in fem.mesh.nodes] )
  # No nodal attributes to add (no postprocessing data)

  # Write element id map
  exo.put_elem_id_map( list(fem.mesh.elements.keys()) )

  # Write element block names/ Write element block properties/ Write element connectivity
  for i,section in enumerate(fem.sections):
    elem_blk_id = i+1
    abaqus_type = fem.sections[section].eltype
    elem_type = equiv_exo_element[abaqus_type]
    blk_elems = fem.mesh.element_sets[section]
    num_blk_elems = len(blk_elems)
    num_elem_nodes = nodes_in_element[abaqus_type]
    num_elem_attrs = 0
    exo.put_elem_blk_info(elem_blk_id, elem_type, num_blk_elems,
                            num_elem_nodes, num_elem_attrs)
    exo.put_elem_blk_name(elem_blk_id,section)
    elem_conn = [ fem.mesh.elements[element].nodes[i] 
                                  for element in blk_elems
                                  for i in equiv_exo_conn[abaqus_type] ]
    exo.put_elem_connectivity(elem_blk_id, elem_conn)

  # Close EXODUS II file
  exo.close()

# Remaining ToDo:
# Write element block attributes
# Write individual node sets
# Write node set names
# Add nodeset attributes
# Write individual side sets
# Write side set names
