# Gerald Pekmezi
# Based partially on the abaqus parser for JuliaFEM 
# https://github.com/JuliaFEM/AbaqusReader.jl
# Only "flat" input files supported
# License is MIT
import os, re
import numpy as np
from classes import *
from abaqusKeywords import *
from mapDicts import *

def parse_model(inputFile):
    model_path = os.path.dirname(inputFile)
    model_name = os.path.basename(inputFile).split('.')[0]
    model = Model(model_path, model_name, parse_mesh(inputFile))

    print("Mesh processing complete!\n\nNow processing model:")
    state = AbaqusReaderState(None, None, None, None, [])

    with open(inputFile) as abaqus_file:
        input_lines = abaqus_file.readlines()
    for line in input_lines:
        if is_comment(line): continue
        elif is_new_section(line):
            begin_new_section(model, state, line)
        else:
            process_line(model, state, line)
    maybe_close_section(model, state)

    return model

def parse_mesh(inputFile):
    print("Processing mesh.")
    mesh = Mesh()

    with open(inputFile) as abaqus_file:
        input_lines = abaqus_file.readlines()
    keyword_indexes = find_keywords(input_lines)
    nkeyword_indexes = len(keyword_indexes)
    # keyword_indexes.append(len(input_lines)+1)
    idx_start = keyword_indexes[0]

    for idx_end in keyword_indexes[1:]:
        keyword_line = input_lines[idx_start].strip().upper()
        keyword = re.findall(r"\s*([\w ]+)", keyword_line)[0].strip()
        if keyword in mesh_keywords:
            print(keyword)
            parse_mesh_section(mesh, input_lines, idx_start, idx_end, keyword)
        elif keyword in abaqus_keywords:
            # print("Non-mesh keyword", keyword,"will be processed later")
            pass
        elif keyword in unsupported_keywords:
            pass
            # print("Non-mesh keyword: ", keyword,"is currently unsupported")
        else:
            print("Keyword: ", keyword,"is currently not recognized as mesh or model keyword")
        idx_start = idx_end
    return mesh

def find_keywords(lines):
    indexes = []
    for idx, line in enumerate(lines):
        if line.startswith("*") and not line.startswith("**"):
            indexes.append(idx)
    return indexes

def is_comment(line):
    return ( line.startswith("**") or len(line.strip()) == 0 )

def is_new_section(line):
    if not is_keyword(line): return False
    section = parse_keyword(line)
    if not is_abaqus_keyword_registered(section.name): return False
    return True

def is_keyword(line):
    return line.startswith("*") and not is_comment(line)

def is_abaqus_keyword_registered(sectionName):
    return sectionName in [*abaqus_keywords,*mesh_keywords,*unsupported_keywords]

def parse_keyword(line, uppercase_keyword=True):
    words = line.split(",")
    for idx, word in enumerate(words): words[idx] = word.strip()
    keyword_name = words[0].strip('*')
    if uppercase_keyword:
        keyword_name = keyword_name.upper()
    keyword = Keyword(keyword_name, {})
    for option in words[1:]:
        pair = option.split("=")
        if uppercase_keyword:
            for i in range(len(pair)): pair[i] = pair[i].upper()
        if len(pair) == 1:
            keyword.options[pair[0]] = pair[0]
        elif len(pair) == 2:
            # keyword.options.extend(pair)
            keyword.options[pair[0]] = pair[1]
        else:
            raise NameError("Keyword failure: ", line, option, pair)
    return keyword

def begin_new_section(model, state, line):
    maybe_close_section(model, state)
    state.data = []
    state.section = parse_keyword(line)
    maybe_open_section(model, state)

def maybe_close_section(model, state):
    if state.section is None: return
    if state.section.name in mesh_keywords: return
    # section_name = state.section.name
    elif state.section.name in unsupported_keywords:
        print("Currently ignored section: ", state.section.name)
    close_section(model, state)
    state.section = None    

def maybe_open_section(model, state):
    section_name = state.section.name
    section_options = state.section.options
    if section_name in mesh_keywords:
        # print("Mesh section: ", section_name, "already processed")
        return
    if section_name in ["STATIC","DYNAMIC"]:
        if state.step is not None:
            print("The current STEP is determined to be ", section_name)
        else:
            raise NameError("*STATIC or *DYNAMIC Section outside *STEP ?")
    if state.section.name not in unsupported_keywords:
        print("New section: ", section_name, " with options ", section_options)
    open_section(model, state)

def process_line(model, state, line):
    if state.section is None:
        print("section = nothing! line = ", line)
        return
    elif is_keyword(line):
        print("missing keyword? line = ", line)
        # close section, this is probably keyword and collecting data should stop.
        maybe_close_section(model, state)
        return
    state.data.append(line)

def get_data(state, types, method):
    data = []
    for row in state.data:
        row = row.strip(' ,\n')
        # print(row)
        col = row.split(',')
        if len(types) == 1:
            # col = [float(elem) for elem in col]
            for i in range(len(col)):
                # print('col[i] = %s(col[i])' %types[0])
                exec('col[i] = %s(col[i])' %types[0])
            # print('col = [%s(elem) for elem in col]' %types[0])
            # exec('col = [%s(elem) for elem in col]' %types[0])
        elif len(types) == len(col):
            for i in range(len(col)):
                # print('col[i] = %s(col[i])' %types[i])
                exec('col[i] = %s(col[i])' %types[i])
        else:
            raise NameError("Wrong number of types")
        # print(col)
        exec('data.%s(col)' %method)
    return data

def get_options(state):
    return state.section.options

def get_option(state, what):
    return get_options(state).get(what)

def open_section(model, state):
    if state.section.name in ELEMENT_SECTIONS:
        element_set = get_option(state, "ELSET")
        print(model.mesh.element_sets,model.mesh.node_sets)
        material_name = get_option(state, "MATERIAL")
        element_type = model.mesh.elements[model.mesh.element_sets[element_set][0]].type
        print(element_set, material_name)
        if state.section.name == "SOLID SECTION":
            state.property = SolidSection(element_type,material_name)
        elif state.section.name == "SHELL SECTION":
            if "COMPOSITE" in state.section.options:
                raise NameError("COMPOSITE shells currently not supported")
            state.property = ShellSection(element_type,material_name)
        elif state.section.name == "BEAM SECTION":
            beam_type = get_option(state, "SECTION")
            state.property = BeamSection(element_type,material_name,beam_type)
        model.sections.update({element_set:state.property})
    elif state.section.name in GENERAL_ELEMENT_SECTIONS:
        raise NameError("General beam/shell sections currently not supported")
    elif state.section.name == "COUPLING":
        constraint_name = get_option(state, "CONSTRAINT NAME")
        ref_node = get_option(state, "REF NODE")
        surface = get_option(state, "SURFACE")
        if not hasattr(state, "coupling"):
            setattr(state, "coupling", [])
        state.coupling = Coupling(None,constraint_name,ref_node,surface,[])
        if not hasattr(model, "coupling"):
            setattr(model, "coupling", [])
        model.coupling.append(state.coupling)
    elif state.section.name == "MATERIAL":
        material_name = get_option(state, "NAME")
        state.material = Material(material_name)
        model.materials.update({material_name:state.material})
    elif state.section.name == "STEP":
        state.step = Step(None, [], [], True)
        model.steps.append(state.step)
    else:
        # print("no open_section() found for ", state.section.name)
        pass

def close_section(model, state):
    if state.section.name in ELEMENT_SECTIONS:
        state.property = None
    elif state.section.name == "DENSITY":
        if len(state.section.options) != 0:
            print("Density options not currently supported")
        # print(get_data(state,['float'],'extend'))
        data = get_data(state,['float'],'extend')
        setattr(state.material, "density", data[0])
    elif state.section.name == "TRANSVERSE SHEAR STIFFNESS":
        print("TRANSVERSE SHEAR STIFFNESS is currently ignored")
    elif state.section.name == "ELASTIC":
        # assert len(state) == 1, "State Number is not 1"
        if len(state.section.options) == 0:
            print("Default Option for Elastic Material is ISOTROPIC")
            state.section.options["TYPE"] = "ISOTROPIC"
        if state.section.options["TYPE"].startswith("ISO"):
            # print(get_data(state,['float'],'extend'))
            material_property = Elastic_Isotropic(get_data(state,['float'],'extend'))
            # print(material_property)
        elif state.section.options["TYPE"] == "ENGINEERING CONSTANTS":
            material_property = Elastic_Eng_Constants(get_data(state,['float'],'extend'))
        elif state.section.options["TYPE"] == "LAMINA":
            material_property = Elastic_Lamina(get_data(state,['float'],'extend'))
        elif state.section.options["TYPE"].startswith("ANISO"):
            material_property = Elastic_Anisotropic(get_data(state,['float'],'extend'))
        else:
            raise NameError("Elastic Option not Implemented :", state.section.options)
        setattr(state.material, "elastic_properties", material_property.params)
        setattr(state.material, "elastic_type", material_property.kind)
    elif state.section.name == "PLASTIC":
        # assert len(state) == 1, "State Number is not 1"
        if len(state.section.options) == 0:
            print("Default Option for Plastic Material is ISOTROPIC")
            state.section.options["HARDENING"] = "ISOTROPIC"
        if state.section.options["HARDENING"].startswith("ISO"):
            # print(get_data(state,['float'],'extend'))
            material_property = Plastic_Isotropic(get_data(state,['float'],'extend'))
        elif state.section.options["HARDENING"] == "JOHNSON COOK":
            material_property = Plastic_JohnsonCook(get_data(state,['float'],'extend'))
        else:
            raise NameError("Plastic Option not Implemented :", state.section.options)
        setattr(state.material, "plastic_properties", material_property)
        # setattr(state.material.plastic_properties, "type", material_property.kind)
        setattr(state.material, "plastic_type", material_property.kind)
    elif state.section.name in ["KINEMATIC","DISTRIBUTING"]:
        data = get_data(state,['int'],'append')
        state.coupling.type = state.section.name
        state.coupling.dofs = np.intp(data[0][1:])
        print("Close section: ", "COUPLING")
    elif state.section.name == "MPC":
        data = get_data(state,['str'],'append')
        # print(data)
        if not hasattr(state, "mpc"):
            setattr(state, "mpc", [])
        print(data)
        state.mpc.append( MPC(data[0][0].upper(), np.intp(data[0][1:])) ) 
    elif state.section.name in BOUNDARY_CONDITIONS:
        # if state.section.name == 'BOUNDARY':
        #      data = get_data(state,['str'],'append')
        # elif state.section.name == 'CLOAD':
        #      data = get_data(state,['int','int','float'],'append')
        # elif state.section.name == 'DLOAD':
        #      data = get_data(state,['int','str','float'],'append')
        data = get_data(state,['str'],'append')
        options = get_options(state)
        bc = Boundary_Condition(state.section.name, data, options)
        if state.step is None:
            model.boundary_conditions.append(bc)
        else:
            state.step.boundary_conditions.append(bc)
    elif state.section.name in OUTPUT_REQUESTS:
        print("Output requests not currently parsed")
    elif state.section.name == "STATIC":
        state.step.kind = "STATIC"
    elif state.section.name == "DYNAMIC":
        state.step.kind = "DYNAMIC"
    elif state.section.name == "END STEP":
        print("Close section: ", "STEP")
        state.step = None
    else:
        # print("no close_section() found for ", state.section.name)
        return

def parse_definition(definition):
    set_defs = {}
    rexp = r"([\w\_\-]+[ ]*=[ ]*[\w\_\-]+)"
    # set_definition = [match.group() for match in re.finditer(rexp, definition)]
    set_definition = re.findall(rexp, definition)
    if len(set_definition) == 0: return None
    for x in set_definition:
        name, vals = [ y.strip() for y in x.split("=") ]
        set_defs[name.lower()] = vals
    return set_defs

def add_set(mesh, definition, mesh_key, abaqus_key, ids):
    has_set_def = parse_definition(definition)
    # print(definition)
    if abaqus_key.upper() == "NSET" and has_set_def is None:
        has_set_def = ['NSET','NALL']
    if abaqus_key in has_set_def:
        set_name = has_set_def[abaqus_key]
        print("Adding ", abaqus_key,": ", set_name)
        # exec('mesh.%s.append(Mesh.%s("%s",ids))' %(mesh_key, mesh_key, set_name))
        exec('mesh.%s.update({"%s":[*ids]})' %(mesh_key, set_name))

def elementProcessor(arr, start, stop):
    idx = start - 1
    def _it():
        nonlocal idx
        idx += 1
        if idx >= stop:
            return None
        return arr[idx]
    return _it

def parse_numbers(line, dtype):
    rexp = r"[0-9]+"
    # matches = [match.group() for match in re.finditer(rexp, line)]
    # print(line)
    matches = re.findall(rexp, line)
    matches = [ (int(match) if dtype=='int' else float(match)) for match in matches ]
    return matches

def parse_mesh_section(mesh, lines, idx_start, idx_end, key):
    definition = lines[idx_start]
    # print(key)
    if key == "NODE":
        nnodes = 0
        ids = []
        for line in lines[idx_start + 1: idx_end]:
            if not is_comment(line):
                rexp = r"[-0-9.eE+]+"
                # m = [match.group() for match in re.finditer(r"[-0-9.eE+]+", line)]
                m = re.findall(rexp, line)
                node_id = int(m[0])
                coords = [ float(coord) for coord in m[1:] ]
                mesh.nodes.update({node_id:Mesh.nodes(coords)})
                ids.append(node_id)
                nnodes += 1
        print(nnodes," nodes found")
        add_set(mesh, definition, "node_sets", "nset", ids)
    elif key == "ELEMENT":
        ids = []
        rexp = r"TYPE=([\w\-\_]+)"
        m = re.findall(rexp, definition, re.IGNORECASE)
        if m is None: raise NameError("Could not match ", rexp, " to line", definition)
        element_type = m[0].upper()
        if element_type not in nodes_in_element.keys(): 
            raise NameError('Element type ', element_type,' not currently supported')
        eltype_nodes = nodes_in_element[element_type]
        # print("Parsing elements. Abaqus element type is ", element_type)
        line_iterator = elementProcessor(lines, idx_start+1, idx_end)
        line = line_iterator()
        while line is not None:
            numbers = parse_numbers(line, 'int')
            if not is_comment(line):
                id = numbers[0]
                ids.append(id)
                connectivity = numbers[1:]
                while len(connectivity) != eltype_nodes:
                    assert len(connectivity) < eltype_nodes
                    line = line_iterator()
                    numbers = parse_numbers(line, 'int')
                    connectivity.extend(numbers)
                mesh.elements.update({id:Mesh.elements(element_type,connectivity)})
            line = line_iterator()
        print(len(mesh.elements)," elements of type",element_type,"found") 
        add_set(mesh, definition, "element_sets", "elset", ids)
    elif key in MESH_SETS:
        data = []
        set_type = "node_sets" if key == "NSET" else "element_sets"
        set_elements = "nodes" if key == "NSET" else "elements"
        rexp = r"((?<=NSET=)([\w\-\_]+)|(?<=NSET=\")([\w\-\_\ ]+)(?=\"))" if key == "NSET" \
                else r"((?<=ELSET=)([\w\-\_]+)|(?<=ELSET=\")([\w\-\_\ ]+)(?=\"))"
        set_name = re.findall(rexp, definition, re.IGNORECASE)[0][0]
        print("Creating ", key.lower(), set_name)
        if definition.upper().strip().endswith("GENERATE"):
            line = lines[idx_start + 1]
            setinp = parse_numbers(line, 'int')
            first_id, last_id, step_ = setinp if len(setinp)==3 else (*setinp,1)
            set_ids = [idx for idx in range(first_id,last_id+1,step_)]
            data.extend(set_ids)
        else:
            for line in lines[idx_start + 1: idx_end+1]:
                if not is_comment(line):
                    set_ids = parse_numbers(line, 'int')
                    data.extend(set_ids)
        # exec('add_set(mesh, definition, "%s", "%s", data)' %(set_type,set_name))
        exec('mesh.%s.update({"%s":%s})' %(set_type,set_name,data))
        # add_set(mesh, definition, "element_sets", "elset", ids)
        # exec('mesh.%s.update({"%s":[ids]})' %(mesh_key, set_name))
    elif key == "SURFACE":
        data = []   #Vector{Tuple{Int, Symbol}}()
        has_set_def = parse_definition(definition)
        if has_set_def == None: return
        set_type = has_set_def.get("type", "ELEMENT")
        set_name = has_set_def.get("name").upper()
        for line in lines[idx_start + 1: idx_end]:
            if is_comment(line): continue
            # print(line)
            if set_type == "ELEMENT":
                rexp = r"(?P<element_id>\d+),.*(?P<element_side>S\d+).*"
            elif set_type == "NODE":
                rexp = r"(?P<element_id>\d+),.*(?P<element_side>\d+\.\d+).*"
            m = re.findall(rexp, line, re.IGNORECASE)
            # print(key,m)
            element_id = int(m[0][0])
            element_side = float(m[0][1]) if set_type == "NODE" else m[0][1]
            # print(element_id, element_side)
            data.append([element_id, element_side])
        mesh.surface_sets.append(Mesh.surface_sets(set_name,data,set_type))
    elif key in MESH_GEN:
        raise NameError("Generation via NGEN/NFILL/ELGEN not implemented")
