from setuptools import find_packages, setup(
    name='abaqus2sierra',
    packages=find_packages(),
    version='0.1.0',
    description='Python Abaqus Parser for Sierra/Exodus II',
    author='Gerald Pekmezi',
    license='MIT',
)